import React, { useState } from "react";
import styled from "styled-components";
import Button from "./components/Button/Button";
import ModalImage from "./components/Modal/ModalImage/ModalImage";
import ModalText from "./components/Modal/ModalText/ModalText";

const ButtonWrapper = styled.div`
display: flex;
justify-content: center;
margin-top: 15px;
gap: 25px;
`;

function App() {
  const [isImageModalOpen, setIsImageModalOpen] = useState(false);
  const [isTextModalOpen, setIsTextModalOpen] = useState(false);

  const openImageModal = () => setIsImageModalOpen(true);
  const closeImageModal = () => setIsImageModalOpen(false);

  const openTextModal = () => setIsTextModalOpen(true);
  const closeTextModal = () => setIsTextModalOpen(false);

  return (
    <>
      <div>
        <ButtonWrapper>
          <Button onClick={openImageModal}>Open first modal</Button>
          <Button onClick={openTextModal}>Open second modal</Button>
        </ButtonWrapper>

        {isImageModalOpen && <ModalImage onClose={closeImageModal} />}
        {isTextModalOpen && <ModalText onClose={closeTextModal} />}
      </div>
    </>
  );
}

export default App
