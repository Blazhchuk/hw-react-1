import styled from "styled-components";

const Header = styled.div`
display: flex;
justify-content: center;
font-family: Inter;
font-size: 32px;
font-weight: 400;
`;

function ModalHeader({ children }) {
    return (
        <Header>{children}</Header>
    );
}

export default ModalHeader;
