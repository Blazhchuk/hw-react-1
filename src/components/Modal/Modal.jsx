import styled from "styled-components";
import ModalWrapper from "./ModalWrapper/ModalWrapper";
// import ModalHeader from "./ModalHeader/ModalHeader";
// import ModalFooter from "./ModalFooter/ModalFooter";
// import ModalClose from "./ModalClose/ModalClose";
// import ModalBody from "./ModalBody/ModalBody";

const Container = styled.div`
padding: 56px 56px;
border-radius: 16px;
background: #FFF;
box-shadow: 0px 4px 4px 0px rgba(0, 0, 0, 0.25);
position: relative;
`;


function Modal({ children, onClose }) {
    const handleWrapperClick = (e) => {
        if (e.target === e.currentTarget) {
            onClose();
        }
    };

    return (
        <ModalWrapper onClick={handleWrapperClick}>
            <Container>
                {children}
            </Container>
        </ModalWrapper>
    );
}


export default Modal;
