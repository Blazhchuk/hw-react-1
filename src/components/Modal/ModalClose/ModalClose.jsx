import styled from "styled-components";

const CloseBtn = styled.p`
cursor: pointer;
display: flex;
justify-content: flex-end;
position: absolute;
top: 20px;
right: 20px;
`;


function ModalClose({ onClick }) {
    return (
        <CloseBtn onClick={onClick}>
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22" fill="none">
                <path d="M18 4L4 18M18 18L4 4.00001" stroke="#3C4242" strokeWidth="1.5" strokeLinecap="round" />
            </svg>
        </CloseBtn>
    );
}

export default ModalClose;
