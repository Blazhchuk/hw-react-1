import styled from "styled-components";
import Modal from "../Modal";
import ModalHeader from "../ModalHeader/ModalHeader";
import ModalBody from "../ModalBody/ModalBody"
import ModalFooter from "../ModalFooter/ModalFooter";
import ModalClose from "../ModalClose/ModalClose";


const ImageWrapper = styled.div`
display: flex;
justify-content: center;
margin-bottom: 40px;
height: 200px;
`;

const imgUrl = [
    './src/assets/iqos.png',
];

function ModalImage({ onClose }) {

    return (
        <Modal onClose={onClose}>
            <ModalClose onClick={onClose} />
            <ImageWrapper>
                <img src={imgUrl} alt="Iqos" />
            </ImageWrapper>
            <ModalHeader>
                <h2>Product Delete!</h2>
            </ModalHeader>
            <ModalBody>
                <p>By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.</p>
            </ModalBody>
            <ModalFooter firstText="NO, CANCEL" secondaryText="YES, DELETE" secondaryClick={onClose} firstClick={onClose} />
        </Modal>
    );
};

export default ModalImage;
