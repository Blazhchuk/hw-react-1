import styled from "styled-components";


const Body = styled.div`
display: flex;
justify-content: center;
color: #7F7F7F;
text-align: center;
font-family: Roboto;
font-size: 16px;
font-weight: 500;
width: 448px;
margin-top: 32px;
`;

function ModalBody({ children }) {
    return (
        <Body>{children}</Body>
    );
}
export default ModalBody;
