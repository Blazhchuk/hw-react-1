import React from "react";
import Modal from "../Modal";
import ModalHeader from "../ModalHeader/ModalHeader";
import ModalBody from "../ModalBody/ModalBody"
import ModalFooter from "../ModalFooter/ModalFooter";
import ModalClose from "../ModalClose/ModalClose";

function ModalText({ onClose }) {

    return (
        <Modal onClose={onClose}>
            <div>
                <ModalClose onClick={onClose} />
                <ModalHeader>Add Product “NAME”</ModalHeader>
                <ModalBody>
                    <p>Description for you product</p>
                </ModalBody>
                <ModalFooter firstText="ADD TO FAVORITE" firstClick={onClose} />
            </div>
        </Modal>
    );
};

export default ModalText;
