import styled from "styled-components";


const Wrapper = styled.div`
position: fixed;
top: 0;
left: 0;
bottom: 0;
right: 0;
background: rgba(0, 0, 0, 0.30);
display: flex;
justify-content: center;
align-items: center;
`;

export default Wrapper;
