import styled from "styled-components";


const StyledButton = styled.button`
background: palevioletred;
border-radius: 10px;
border: none;
color: white;
padding: 15px;
font-size: 16px;
cursor: pointer;

&:hover {
  background: darkviolet;
}
`;

function Button({ type = 'button', className, onClick, children }) {

    return (
        <>
            <StyledButton type={type} className={className} onClick={onClick}>
                {children}
            </StyledButton>
        </>
    );

}

export default Button;
